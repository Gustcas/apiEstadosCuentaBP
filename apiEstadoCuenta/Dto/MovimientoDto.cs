﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiEstadoCuenta.Dto
{
    public class MovimientoDto
    {
        public string NumeroCuenta { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
    }
}
