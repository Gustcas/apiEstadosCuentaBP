﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiEstadoCuenta.Models;
using apiEstadoCuenta.Models.Response;
using apiEstadoCuenta.Models.Request;

namespace apiEstadoCuenta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CuentaController : ControllerBase
    {
        BaseDatosEstadoCuentaGPContext db = new BaseDatosEstadoCuentaGPContext();

        // GET: api/<ValuesController>
        [HttpGet]
        public IActionResult getConsultaCuenta()
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
                var _cuenta = db.Cuenta.Join(db.Cliente, x => x.Clienteid, b => b.Clienteid, (x, b) => new {
                    NumeroCuenta = x.NumeroCuenta,
                    TipoCuenta = x.TipoCuenta,
                    SaldoInicial = x.SaldoInicial,
                    Estado = x.Estado,
                    Nombre = b.Nombre
                }).ToList();
                _respuesta.Exito = 1;
                _respuesta.Data = _cuenta;
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
               // throw new Exception("Error message" + ex.Message);
            }
            return Ok(_respuesta);
        }


        // POST api/<ValuesController>
        [HttpPost]

        public IActionResult addCreateCuenta(CuentaRequest _cuentaModel)
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
                Cuenta _cuenta = new Cuenta();
                _cuenta.NumeroCuenta = _cuentaModel.NumeroCuenta;
                _cuenta.TipoCuenta = _cuentaModel.TipoCuenta;
                _cuenta.SaldoInicial = _cuentaModel.SaldoInicial;
                _cuenta.Estado = _cuentaModel.Estado;
                _cuenta.Clienteid = _cuentaModel.Clienteid;
                db.Cuenta.Add(_cuenta);
                db.SaveChanges();
                _respuesta.Exito = 1;
                _respuesta.Mensaje = "Registro creado correctamente";
                _respuesta.Data = _cuenta;

            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
                //throw new Exception("Error message" + ex.Message);
            }
            return Ok(_respuesta);
        }

        // PUT api/<ValuesController>
        [HttpPut]
        public IActionResult editCuenta(CuentaRequest _cuentaModel)
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
                Cuenta _cuenta = db.Cuenta.Find(_cuentaModel.IdCuenta);
                _cuenta.NumeroCuenta = _cuentaModel.NumeroCuenta;
                _cuenta.TipoCuenta = _cuentaModel.TipoCuenta;
                _cuenta.SaldoInicial = _cuentaModel.SaldoInicial;
                _cuenta.Estado = _cuentaModel.Estado;
                _cuenta.Clienteid = _cuentaModel.Clienteid;
                db.Entry(_cuenta).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                _respuesta.Exito = 1;
                _respuesta.Mensaje = "Registro actualizado correctamente";
                _respuesta.Data = _cuenta;
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
                //throw new Exception("Error message" + ex.Message);
            }
            return Ok(_respuesta);

        }


        // DELETE api/<ValuesController>/5
        [HttpDelete("{_idCuenta}")]
        public IActionResult deleteCuenta(int _idCuenta)
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
                Cuenta _cuenta = db.Cuenta.Find(_idCuenta);
                db.Remove(_cuenta);
                db.SaveChanges();
                _respuesta.Exito = 1;
                _respuesta.Mensaje = "Registro eliminado correctamente";
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
                //throw new Exception("Error message" + ex.Message);
            }
            return Ok(_respuesta);

        }


    }
}
