﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiEstadoCuenta.Models;
using apiEstadoCuenta.Models.Response;
using apiEstadoCuenta.Models.Request;
using apiEstadoCuenta.Dto;
using Microsoft.EntityFrameworkCore;

namespace apiEstadoCuenta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovimientoController : ControllerBase
    {
        BaseDatosEstadoCuentaGPContext db = new BaseDatosEstadoCuentaGPContext();

        // GET: api/<ValuesController>
        [HttpGet]
        public IActionResult getConsultaMovimientos()
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
               //var _movimiento = db.Movimiento.ToList();

                var _movimiento = db.Cuenta.Join(db.Movimiento, x => x.IdCuenta, b => b.IdCuenta, (x, b) => new {
                    IdMovimiento = b.IdMovimiento,
                    Fecha=b.Fecha,
                    TipoMovimiento = b.TipoMovimiento,
                    Valor = b.Valor,
                    Saldo=b.Saldo,
                    NumeroCuenta = x.NumeroCuenta                  
                }).ToList();
                _respuesta.Exito = 1;
                _respuesta.Data = _movimiento;
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
                //throw new Exception("Error message" + ex.Message);
            }

            return Ok(_respuesta);            
        }

        // POST api/<ValuesController>
        [HttpPost]

        public IActionResult addCreateMovimiento(MovimientoRequest _movimientoModel)
        {
            Respuesta _respuesta = new Respuesta();
            Movimiento _movimiento = new Movimiento();
            try
            {
                //Movimiento _movimiento = new Movimiento();                
                _movimiento.Fecha = DateTime.Today;
                _movimiento.TipoMovimiento = _movimientoModel.TipoMovimiento.Trim();           
                _movimiento.Valor = _movimientoModel.Valor;
                Cuenta _cuenta = (from c in db.Cuenta
                                       where c.NumeroCuenta.Trim() == _movimientoModel.NumeroCuenta.Trim()
                                       select c).Single();   

                if (_movimiento.TipoMovimiento.Equals("Retiro"))
                {
                    
                    if (_cuenta.SaldoInicial>0)
                    {
                        _movimiento.Saldo = _cuenta.SaldoInicial - _movimiento.Valor;
                        _movimiento.IdCuenta = _cuenta.IdCuenta;
                        _cuenta.SaldoInicial = _movimiento.Saldo;
                        db.Entry(_cuenta).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        db.Movimiento.Add(_movimiento);
                        db.SaveChanges();
                        _respuesta.Exito = 1;
                        _respuesta.Mensaje = "Retiro exitoso";
                        _respuesta.Data = _movimiento;
                    }
                    else
                    {                        
                        _respuesta.Mensaje = "Saldo no disponible";
                        _respuesta.Data = null;                   
                        
                    }                     
                }
                else if (_movimiento.TipoMovimiento.Equals("Deposito"))
                {
                    _movimiento.Saldo = _cuenta.SaldoInicial + _movimiento.Valor;
                    _movimiento.IdCuenta = _cuenta.IdCuenta;
                    _cuenta.SaldoInicial = _movimiento.Saldo;
                    db.Entry(_cuenta).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.Movimiento.Add(_movimiento);
                    db.SaveChanges();
                    _respuesta.Exito = 1;
                    _respuesta.Mensaje = "Deposito exitoso";
                    _respuesta.Data = _movimiento;
                }              
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
            }
            return Ok(_respuesta);
        }


        // POST api/<ValuesController>
        [HttpPost]
        [Route("reporte")]
        public IActionResult reporte(MovimientoDto _movimientoDto)
        {
            Respuesta _respuesta = new Respuesta();    
            try
            {                
                var _reporteMovimientos = (from m in db.Movimiento
                                join cu in db.Cuenta on m.IdCuenta equals cu.IdCuenta
                                join c in db.Cliente on cu.Clienteid equals c.Clienteid
                                where cu.NumeroCuenta == _movimientoDto.NumeroCuenta
                                && m.Fecha >= DateTime.Parse(_movimientoDto.FechaInicio)
                                && m.Fecha <= DateTime.Parse(_movimientoDto.FechaFin)
                                 select new
                                {
                                    Fecha=m.Fecha,
                                    Cliente=c.Nombre,
                                    NumeroCuenta=cu.NumeroCuenta,
                                    TipoCuenta= cu.TipoCuenta,
                                    SaldoInicial= cu.SaldoInicial,
                                    Estado=cu.Estado,
                                    TipoMovimiento=m.TipoMovimiento,
                                    Movimiento = m.Valor,
                                    SaldoDiponible=m.Saldo
                                }).ToList();                        
                _respuesta.Exito = 1;
                _respuesta.Mensaje = "Reporte Generado con exito";
                _respuesta.Data = _reporteMovimientos;
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
            }
            return Ok(_respuesta);
        }







    }
}
