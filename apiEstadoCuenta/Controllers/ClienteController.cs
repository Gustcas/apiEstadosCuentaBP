﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiEstadoCuenta.Models;
using apiEstadoCuenta.Models.Response;
using apiEstadoCuenta.Models.Request;

namespace apiEstadoCuenta.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        BaseDatosEstadoCuentaGPContext db = new BaseDatosEstadoCuentaGPContext();

        // GET: api/<ValuesController>
        [HttpGet]
        public IActionResult getConsultaCliente()
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
                var _cliente = (from c in db.Cliente
                                where c.Estado == true
                                select c).ToList();
                _respuesta.Exito = 1;
                _respuesta.Data = _cliente;
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
                //throw new Exception("Error message"+ex.Message);
            }
            return Ok(_respuesta);
        }



        // POST api/<ValuesController>
        [HttpPost]
        public IActionResult addCreateCliente(ClienteRequest _clienteModel)
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
                Cliente _cliente = new Cliente();
                _cliente.Nombre = _clienteModel.Nombre;
                _cliente.Genero = _clienteModel.Genero;
                _cliente.Edad = _clienteModel.Edad;
                _cliente.Identificacion = _clienteModel.Identificacion;
                _cliente.Direccion = _clienteModel.Direccion;
                _cliente.Telefono = _clienteModel.Telefono;
                _cliente.Password = _clienteModel.Password;
                _cliente.Estado = _clienteModel.Estado;
                db.Cliente.Add(_cliente);
                db.SaveChanges();
                _respuesta.Exito = 1;
                _respuesta.Mensaje = "Registro creado correctamente";
                _respuesta.Data = _cliente;
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
                //throw new Exception("Error message" + ex.Message);
            }
            return Ok(_respuesta);

        }

        // PUT api/<ValuesController>
        [HttpPut]
        public IActionResult editCliente(ClienteRequest _clienteModel)
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
                Cliente _cliente = db.Cliente.Find(_clienteModel.Clienteid);
                _cliente.Nombre = _clienteModel.Nombre;
                _cliente.Genero = _clienteModel.Genero;
                _cliente.Edad = _clienteModel.Edad;
                _cliente.Identificacion = _clienteModel.Identificacion;
                _cliente.Direccion = _clienteModel.Direccion;
                _cliente.Telefono = _clienteModel.Telefono;
                _cliente.Password = _clienteModel.Password;
                _cliente.Estado = _clienteModel.Estado;
                db.Entry(_cliente).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                _respuesta.Exito = 1;
                _respuesta.Mensaje = "Registro actualizado correctamente";
                _respuesta.Data = _cliente;
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
                //throw new Exception("Error message" + ex.Message);
            }
            return Ok(_respuesta);

        }


        // DELETE api/<ValuesController>/5
        [HttpDelete("{_clienteid}")]
        public IActionResult deleteCliente(int _clienteid)
        {
            Respuesta _respuesta = new Respuesta();
            try
            {
                Cliente _cliente = db.Cliente.Find(_clienteid);
                db.Remove(_cliente);
                db.SaveChanges();
                _respuesta.Exito = 1;
                _respuesta.Mensaje = "Registro eliminado correctamente";
            }
            catch (Exception ex)
            {
                _respuesta.Mensaje = ex.Message;
                //throw new Exception("Error message" + ex.Message);
            }
            return Ok(_respuesta);

        }


    }
}
