﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiEstadoCuenta.Models.Request
{
    public class MovimientoRequest
    {
        public int IdMovimiento { get; set; }
        public DateTime? Fecha { get; set; }
        public string TipoMovimiento { get; set; }
        public int? Valor { get; set; }
        public int? Saldo { get; set; }
        public string NumeroCuenta { get; set; }
        public int? IdCuenta { get; set; }
    }
}
