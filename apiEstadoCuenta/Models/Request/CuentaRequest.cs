﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiEstadoCuenta.Models.Request
{
    public class CuentaRequest
    {
        public int IdCuenta { get; set; }
        public string NumeroCuenta { get; set; }
        public string TipoCuenta { get; set; }
        public int? SaldoInicial { get; set; }
        public bool? Estado { get; set; }
        public int? Clienteid { get; set; }
    }
}
