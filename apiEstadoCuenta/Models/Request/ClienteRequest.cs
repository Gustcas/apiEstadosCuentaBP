﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiEstadoCuenta.Models.Request
{
    public class ClienteRequest
    {
        public int Clienteid { get; set; }
        public string Nombre { get; set; }
        public string Genero { get; set; }
        public int? Edad { get; set; }
        public string Identificacion { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Password { get; set; }
        public bool? Estado { get; set; }

    }
}
