﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace apiEstadoCuenta.Models
{
    public partial class Movimiento
    {
        public int IdMovimiento { get; set; }
        public DateTime? Fecha { get; set; }
        public string TipoMovimiento { get; set; }
        public int? Valor { get; set; }
        public int? Saldo { get; set; }
        public int? IdCuenta { get; set; }
        public virtual Cuenta IdCuentaNavigation { get; set; }
    }
}
