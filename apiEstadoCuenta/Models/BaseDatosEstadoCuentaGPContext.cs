﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace apiEstadoCuenta.Models
{
    public partial class BaseDatosEstadoCuentaGPContext : DbContext
    {
        public BaseDatosEstadoCuentaGPContext()
        {
        }

        public BaseDatosEstadoCuentaGPContext(DbContextOptions<BaseDatosEstadoCuentaGPContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<Cuenta> Cuenta { get; set; }
        public virtual DbSet<Movimiento> Movimiento { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-H2CILCV\\SQLEXPRESS;Database=BaseDatosEstadoCuentaGP;User ID=sa;Password=Diospatria1.2@;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.ToTable("cliente");

                entity.Property(e => e.Clienteid).HasColumnName("clienteid");

                entity.Property(e => e.Direccion)
                    .HasColumnName("direccion")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Edad).HasColumnName("edad");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.Genero)
                    .HasColumnName("genero")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Identificacion)
                    .HasColumnName("identificacion")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasColumnName("nombre")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasColumnName("telefono")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Cuenta>(entity =>
            {
                entity.HasKey(e => e.IdCuenta);

                entity.ToTable("cuenta");

                entity.Property(e => e.IdCuenta).HasColumnName("idCuenta");

                entity.Property(e => e.Clienteid).HasColumnName("clienteid");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.NumeroCuenta)
                    .HasColumnName("numeroCuenta")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SaldoInicial).HasColumnName("saldoInicial");

                entity.Property(e => e.TipoCuenta)
                    .HasColumnName("tipoCuenta")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Cliente)
                    .WithMany(p => p.Cuenta)
                    .HasForeignKey(d => d.Clienteid)
                    .HasConstraintName("FK_cuenta_cliente");
            });

            modelBuilder.Entity<Movimiento>(entity =>
            {
                entity.HasKey(e => e.IdMovimiento);

                entity.ToTable("movimiento");

                entity.Property(e => e.IdMovimiento).HasColumnName("idMovimiento");

                entity.Property(e => e.Fecha)
                    .HasColumnName("fecha")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdCuenta).HasColumnName("idCuenta");

                entity.Property(e => e.Saldo).HasColumnName("saldo");

                entity.Property(e => e.TipoMovimiento)
                    .HasColumnName("tipoMovimiento")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Valor).HasColumnName("valor");

                entity.HasOne(d => d.IdCuentaNavigation)
                    .WithMany(p => p.Movimiento)
                    .HasForeignKey(d => d.IdCuenta)
                    .HasConstraintName("FK_movimiento_cuenta");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
